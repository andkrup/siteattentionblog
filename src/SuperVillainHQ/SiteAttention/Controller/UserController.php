<?php

namespace SuperVillainHQ\SiteAttention\Controller {

	use SuperVillainHQ\SiteAttention\Model\User\UserService;

	/**
	 * Class Comment
	 * @package SuperVillainHQ\SiteAttention\Controller
	 */
	class UserController extends Controller{

		function indexAction(){}

		function getAction(){}

		function editAction(){}

		function updateAction(){
			$userId = intval($this->request->get('userId'));
			$userService = new UserService($userId);

			$data = (object)[
				'password' => trim($this->request->get('password'))
			];
			if($userService->update($data)){
				// ok
				return;
			}
			// not ok
		}

		function createAction(){
			$data = (object)[
				'name' => trim($this->request->get('name')),
				'email' => trim($this->request->get('email'))
			];
			if($userConfirm = UserService::createUserConfirm($data)){
				$this->view->setVar('message', 'Please respond to the email we sent. You will be able to create a password after confirming your email');
			}
		}

		function confirmUserAction(){
			if($user = UserService::confirmUser(trim($this->request->get('token')))){
				// display a form for inputting a password
				$this->view->setVar('userId', $user->id);
			}
		}

		function banAction(){}

		function unBanAction(){}

		function deleteAction(){}
	}
}


