<?php

namespace SuperVillainHQ\SiteAttention\Controller {

	use Phalcon\Mvc\Controller as PhalconController;
	/**
	 * Class Controller
	 * @package SuperVillainHQ\SiteAttention\Controller
	 */
	abstract class Controller extends PhalconController{
	}
}


