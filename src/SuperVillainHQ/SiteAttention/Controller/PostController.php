<?php

namespace SuperVillainHQ\SiteAttention\Controller {

	use SuperVillainHQ\SiteAttention\Model\Post\PostService;

	/**
	 * Class Post
	 * @package SuperVillainHQ\SiteAttention\Controller
	 */
	class PostController extends Controller{

		function indexAction(){}

		function getAction($id){

		}

		function latestAction(){
			$this->view->setVar('parentId', null);
		}

		function createAction(){
			// fake login
			$userId = 1;
			if($this->request->isPost()){
				$data = (object)[
					'userId' => $userId,
					'parent' => $this->request->has('parentId') ? intval($this->request->get('parentId')) : null,
					'title' => htmlentities(trim($this->request->get('title'))),
					'raw' => htmlentities(trim($this->request->get('body')))
				];
				if($post = PostService::create($data)){
					$this->response->redirect("post/{$post->id}");
					return;
				}
				echo "failed creating post";
				exit;
			}
		}

		function createRevisionAction(){}

		function rollBackAction(){}

		function editAction(){}

		function archiveAction(){}

		function unArchiveAction(){}

		function publishAction(){}

		function unPublishAction(){}

		function moderateAction(){}

		function deleteAction(){}
	}
}


