<?php

namespace SuperVillainHQ\SiteAttention\Controller {

	/**
	 * Class ErrorController
	 * @package SuperVillainHQ\SiteAttention\Controller
	 */
	class ErrorController extends Controller{

		function indexAction(){
			var_dump($this->dispatcher->getParam('error'));
		}

		function error404Action(){
			var_dump($this->dispatcher->getParam('error'));
		}
	}
}


