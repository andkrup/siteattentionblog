<?php

namespace SuperVillainHQ\SiteAttention {

	use Phalcon\Config;
	use Phalcon\Di;
	use Phalcon\DiInterface;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Events\Event;
	use Phalcon\Mvc\Url as UrlResolver;
	use Phalcon\Loader;
	use Phalcon\Mvc\Router;
	use Phalcon\Mvc\Application;
	use Phalcon\Mvc\View;

	/**
	 * Class BlogApplication
	 * @package SuperVillainHQ\SiteAttention
	 */
	class BlogApplication{
		private static $internalApp;
		private static $basePath;

		static function absPath(string $path):string{
			if(strpos($path, '/') === 0){
				return realpath($path);
			}
			$basePath = rtrim(self::$basePath, '/');
			$path = rtrim($path, '/');
			$rpath = "{$basePath}/{$path}";
			return realpath($rpath);
		}

		public static function run(string $configFilePath){
			$di = new Di\FactoryDefault();

			if(is_file($configFilePath) && is_readable($configFilePath)){
				$config = new Config(parse_ini_file($configFilePath, true));
				self::$basePath = realpath(dirname(dirname($configFilePath))); // 2 up

				$di->set('config', $config);

				$dir = new \DirectoryIterator(self::$basePath . "/{$config->environment->servicesPath}");
				foreach ($dir as $file) {
					if($file instanceof \SplFileInfo){
						if(!$file->isDot()){
							$path = $file->getRealPath();
//							require_once $path;
							$fqdn = str_replace('/', '\\', str_replace(self::$basePath . '/src/', '', str_replace('.php', '', $path)));
							if($fqdn == 'SuperVillainHQ\SiteAttention\Services\Service'){
								continue;
							}
							$reflector = new \ReflectionClass($fqdn);
							if($reflector->implementsInterface('SuperVillainHQ\SiteAttention\Services\Service')){
								$service = $reflector->newInstance();
								$shared = $service->isShared();
								$name = $service->getName();
								$definition = $service->definition();
								$di->set($name, $definition, $shared);
							}
						}
					}
				}

				$di->set('router', function () {
					$router = new Router(false);

					$router->setDefaultModule('blog');
					$router->setDefaultController('post');
					$router->setDefaultAction('latest');
					$router->removeExtraSlashes(true);

					self::appendRoutes($router);

					return $router;
				});
				$di->set('url', function () use ($config) {
					$url = new UrlResolver();
					$url->setBaseUri('/');

					return $url;
				}, true);

				self::$internalApp = new Application($di);

				self::$internalApp->registerModules([
					'admin' => [
						'className' => 'SuperVillainHQ\SiteAttention\AdminApplication',
						'path' => __DIR__ . '/AdminApplication.php'
					],
					'blog' => [
						'className' => 'SuperVillainHQ\SiteAttention\BlogApplication',
						'path' => __DIR__ . '/BlogApplication.php'
					]
				]);

				$eventsManager = $di->getShared('eventsManager');
				self::$internalApp->setEventsManager($eventsManager);

				$eventsManager->attach(
					'application:boot',
					function (Event $event, $application) {
						$type = $event->getType();
					}
				);
				$eventsManager->attach(
					'application:beforeHandleRequest',
					function (Event $event, $application) {
						$type = $event->getType();
					}
				);
				$eventsManager->attach(
					'application:afterHandleRequest',
					function (Event $event, $application) {
						$type = $event->getType();
					}
				);


				try{
					$response = self::$internalApp->handle();
					$response->send();
				}
				catch(\Exception $e){
					$response = $di->get('response');
					$response->setStatusCode(500, 'Internal Server Error');
					$response->setContent($e->getMessage());
					$response->send();
				}
			}
			else{
				$response = $di->get('response');
				$response->setStatusCode(500, 'Internal Server Error');
				$response->setContent('config unavailable');
				$response->send();
			}
		}

		private static function appendRoutes(Router &$router){
			$router->add("/:controller/:action/:params", [
				"controller" => 1,
				"action" => 2,
				"params" => 3
			]);
		}

		public function registerAutoloaders(DiInterface $dependencyInjector = null){
//			$loader = new Loader();
//			$config = $dependencyInjector->get('config');
////			$loader->registerDirs([self::absPath($config->blog->controllersPath)]);
//			$loader->register();
		}

		public function registerServices(DiInterface $dependencyInjector){
			$config = $dependencyInjector->get('config');

			$dependencyInjector->set('view', function () use ($config) {
				$viewsDir = self::absPath($config->blog->viewsPath);
				$view = new View();
				$view->setViewsDir($viewsDir);
				$view->registerEngines(['.phtml' => 'Phalcon\Mvc\View\Engine\Php']);

				return $view;
			}, true);

			$dependencyInjector->set('dispatcher', function () use ($dependencyInjector) {
				$dispatcher = new Dispatcher();

				$dispatcher->setDefaultNamespace(
					'SuperVillainHQ\SiteAttention\Controller'
				);

				$evManager = $dependencyInjector->getShared('eventsManager');
				$evManager->attach(
					"dispatch:beforeException",
					function ($event, $dispatcher, $exception) use ($dependencyInjector) {
						$dispatcher->setParam('error', $exception);

						switch ($exception->getCode()) {
							case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
							case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
								$dispatcher->forward([
									'controller' => 'error',
									'action' => 'error404',
								]);
								return false;
						}

						$dispatcher->forward([
							'controller' => 'error',
							'action' => 'index',
						]);
						return false;
					}
				);
				$dispatcher->setEventsManager($evManager);
				return $dispatcher;
			}, true);
		}
	}
}


