<?php

namespace SuperVillainHQ\SiteAttention\Services {

	use Phalcon\Di;
	use Phalcon\Db\Adapter\Pdo\Mysql;

	/**
	 * Class DataBase
	 * @package SuperVillainHQ\SiteAttention\Services
	 */
	class DataBase implements Service{
		public function isShared(){
			return true;
		}

		public function getName(){
			return 'db';
		}

		public function definition(){
			return function(){
				$config = Di::getDefault()->get('config');
				$options = [
					'host' => $config->mysql->host,
					'username' => $config->mysql->user,
					'password' => $config->mysql->password,
					'dbname' => $config->mysql->database,
					"options" => [
						\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4'
					]
				];
				return new Mysql($options);
			};
		}
	}
}


