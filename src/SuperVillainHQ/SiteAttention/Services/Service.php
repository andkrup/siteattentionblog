<?php

namespace SuperVillainHQ\SiteAttention\Services {

	/**
	 * Class Service
	 * @package SuperVillainHQ\SiteAttention\Services
	 */
	interface Service{
		public function isShared();
		public function getName();
		public function definition();
	}
}


