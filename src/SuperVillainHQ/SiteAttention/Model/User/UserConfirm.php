<?php

namespace SuperVillainHQ\SiteAttention\Model\User {

	use Phalcon\Mvc\Model;

	/**
	 * Class UserConfirm
	 * @package SuperVillainHQ\SiteAttention\Model\User
	 */
	class UserConfirm extends Model{

		public $id;
		public $userId;
		public $email;
		public $token;
		public $created;
		public $confirmed;
	}
}


