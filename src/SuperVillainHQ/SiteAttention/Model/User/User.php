<?php

namespace SuperVillainHQ\SiteAttention\Model\User {

	use Phalcon\Mvc\Model;

	/**
	 * Class User
	 * @package SuperVillainHQ\SiteAttention\Model\User
	 */
	class User extends Model{
		public $id;
		public $name;
		public $email;
		public $password;
	}
}


