<?php

namespace SuperVillainHQ\SiteAttention\Model\User {

	use Phalcon\Di;

	/**
	 * Class UserService
	 * @package SuperVillainHQ\SiteAttention\Model\User
	 */
	class UserService{
		/**
		 * @var User
		 */
		private $user;

		function __construct($user){
			if(is_int($user)){
				$user = User::findById(intval($user));
			}
			$this->user = $user;
		}

		static function createUserConfirm(\stdClass $userData){
			$di = Di::getDefault();

			$email = trim($userData->email);
			$user = self::createUser(trim($userData->name), $email);
			$instance = new UserConfirm();
			$instance->token = hash('sha1', time() . rand());
			$instance->userId = $user->id;
			$instance->email = $email;

			if($instance->save()){
				return $instance;
			}
			throw new \Exception("Failed to create user");
		}

		protected static function createUser(string $name, string $email):User{
			$di = Di::getDefault();

			$instance = new User();
			$instance->name = trim($name);
			$instance->email = trim($email);

			if($instance->save()){
				return $instance;
			}
			throw new \Exception("Failed to create user");
		}

		public static function confirmUser(string $token){
			if($instance = UserConfirm::findByToken($token)){
				$di = Di::getDefault();
				// TODO: start session! User instance is available through UserConfirm
				$di->response->redirect('user/update'); // the update-form, including password input
				return;
			}
		}

		public function update(\stdClass $data):bool{
			$di = Di::getDefault();
			if(property_exists($data, 'password')){
				// built-in blowfish hashing
				$this->user->password = $di->security->hash(trim($data->password));
			}

			return $this->user->update();
		}
	}
}


