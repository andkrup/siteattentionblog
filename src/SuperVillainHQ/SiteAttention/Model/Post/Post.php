<?php

namespace SuperVillainHQ\SiteAttention\Model\Post {

	use Phalcon\Mvc\Model;

	/**
	 * Class Post
	 *
	 * A Discussion is basically a tree-structure of Post-nodes
	 * @package SuperVillainHQ\SiteAttention\Model\Post
	 */
	class Post extends Model{

		public $id;
		public $parentId;
		public $userId;
		public $created;
		public $createdTZ;
		public $localeId;
		public $raw;
		public $html;

		public function initialize(){
			$this->setSource("post");
		}


		function comments(){}

		function responses(){}

		function isCommentOn(Post $post):bool{
			// is this instance a child-node of $post?
			throw new \Exception('not yet implemented');
		}

		function hasComments():bool{
			// does this instance have child-nodes
			throw new \Exception('not yet implemented');
		}
	}
}


