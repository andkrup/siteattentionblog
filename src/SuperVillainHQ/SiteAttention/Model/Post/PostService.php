<?php

namespace SuperVillainHQ\SiteAttention\Model\Post {

	/**
	 * Class PostService
	 * @package SuperVillainHQ\SiteAttention\Model\Post
	 */
	class PostService{

		static function create(\stdClass $postData):Post{
			if(is_null($postData->parent) || !intval($postData->parent)){
				if(empty($postData->title)){
					throw new \Exception("A post starting a discussion should have a title");
				}
			}
			if(empty($postData->userId)){
				throw new \Exception("Invalid User");
			}
			if(empty($postData->localeId)){
				$postData->localeId = 1;
			}
			$instance = new Post();
			$instance->assign((array) $postData);
			$instance->save();
			return $instance;
		}
	}
}


