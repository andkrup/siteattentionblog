<?php


use Phinx\Seed\AbstractSeed;

class TestUsers extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(){
    	$data = [
			['id' => 1, 'name' => 'test', 'password' => 'cleartext_remove!', 'email' => 'andkrup@gmail.com'],
		];
		$posts = $this->table('user');
		$posts->insert($data);
		$posts->save();
    }
}
