<?php


use Phinx\Seed\AbstractSeed;

class InitialLocales extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(){
    	$data = [
			['id' => 1, 'iso639_1' => 'en', 'intlName' => 'english'],
			['id' => 2, 'iso639_1' => 'da', 'intlName' => 'dansk']
		];
		$posts = $this->table('locale');
		$posts->insert($data);
		$posts->save();
    }
}
