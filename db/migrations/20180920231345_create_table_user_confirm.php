<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class CreateTableUserConfirm extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('userConfirm')) {
			$table = $this->table('userConfirm', ['id' => false, 'primary_key' => 'id']);

			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('userId', 'integer', ['signed' => false, 'null' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('email', 'string', ['null' => false, 'limit' => 255]);
			$table->addColumn('token', 'string', ['null' => true, 'limit' => 255]);
			$table->addColumn('created', 'datetime', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
			$table->addColumn('confirmed', 'datetime', ['null' => true]);

			$table->addForeignKey('userId', 'user', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION']);

			$table->create();
		}
    }
}
