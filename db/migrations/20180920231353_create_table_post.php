<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class CreateTablePost extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(){
    	if(!$this->hasTable('post')) {
			$table = $this->table('post', ['id' => false, 'primary_key' => 'id']);

			$table->addColumn('id', 'integer', ['signed' => false, 'identity' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('parentId', 'integer', ['signed' => false, 'null' => true, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('userId', 'integer', ['signed' => false, 'null' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('created', 'datetime', ['null' => false, 'default' => 'CURRENT_TIMESTAMP']);
			$table->addColumn('createdTZ', 'string', ['null' => false, 'default' => 'UTC', 'limit' => 32]);
			$table->addColumn('localeId', 'integer', ['signed' => false, 'null' => false, 'limit' => MysqlAdapter::INT_REGULAR]);
			$table->addColumn('title', 'string', ['null' => true, 'limit' => 255]);
			$table->addColumn('raw', 'text', ['null' => true, 'limit' => MysqlAdapter::TEXT_REGULAR]);

			$table->addForeignKey('parentId', 'post', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION']);
			$table->addForeignKey('userId', 'user', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION']);
			$table->addForeignKey('localeId', 'locale', 'id', ['delete'=> 'NO_ACTION', 'update'=> 'NO_ACTION']);

			$table->create();
		}
    }
}
