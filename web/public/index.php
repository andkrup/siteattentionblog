<?php
/**
 * Bootstrap the customers app
 */

require __DIR__ . '/../../vendor/autoload.php';

SuperVillainHQ\SiteAttention\BlogApplication::run(__DIR__ . '/../../config/config.ini');